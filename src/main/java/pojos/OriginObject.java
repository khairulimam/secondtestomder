package pojos;

import lombok.Data;

import java.util.List;

/**
 * Created by khairulimam on 23/04/17.
 */
@Data
public class OriginObject {
    private List<String> _attachments;
    private List<String> _geolocation;
    private String _bamboo_dataset_id;
    private String _deleted_at;
    private String _id;
    private String _status;
    private String _submission_time;
    private String _uuid;
    private String _xform_id_string;
    private String animal_point;
    private String communities_villages;
    private String date;
    private String deviceid;
    private String districts_divisions;
    private String end;
    private String enum_id_1;
    private String formhub_uuid;
    private String grid;
    private String locations_wards;
    private String other_point_1km;
    private String research_asst_name;
    private String respondent;
    private String road_available;
    private String signal;
    private String simserial;
    private String start;
    private String subscriberid;
    private String water_connected;
    private String water_developer;
    private String water_functioning;
    private String water_lift_mechanism;
    private String water_manager;
    private String water_manager_name;
    private String water_pay;
    private String water_point_condition;
    private String water_point_geocode;
    private String water_point_id;
    private String water_point_image;
    private String water_source_type;
    private String water_used_season;

    @Override
    public String toString() {
        return String.format("communityname: %s, condition: %s, water_functioning: %s", communities_villages, water_point_condition, water_functioning);
    }

}

package magic;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import lombok.Data;
import pojos.OriginObject;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by khairulimam on 23/04/17.
 */
@Data
public class WaterDataOperations {
    private List<OriginObject> originObjects;
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public WaterDataOperations(URL dataSetSource) {
        setOriginObjects(dataSetSource);
    }

    public WaterDataOperations() {
    }

    public void setOriginObjects(URL dataSource) {
        try {
            InputStreamReader inputStreamReader = null;
            Logger.getLogger(getClass().getSimpleName()).log(Level.INFO, "Requesting data ...");
            inputStreamReader = new InputStreamReader(dataSource.openStream());
            this.originObjects = gson.fromJson(inputStreamReader, new TypeToken<List<OriginObject>>() {
            }.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map calculate() {
        Map processedData = new HashMap();
        processedData.put("number_of_functional_water_point", this.filterByFunctioningWater().size());
        processedData.put("ranking_and_number_water_points", rankingAndNumberOfEachCommunity());
        return processedData;
    }

    public String calculatedToJson() {
        return gson.toJson(calculate());
    }

    private Map<String, List<OriginObject>> groupDataByCommunityName() {
        Map<String, List<OriginObject>> groupedData = originObjects.stream()
                .collect(Collectors
                        .groupingBy(OriginObject::getCommunities_villages));
        return groupedData;
    }

    private Map<String, Map<String, String>> rankingAndNumberOfEachCommunity() {
        Map<String, Map<String, String>> data = new HashMap<>();
        groupDataByCommunityName().forEach((s, originObjects1) -> {
            Map<String, String> value = new HashMap<>();
            List<OriginObject> brokenWaterPointOfCurrentCommunity = originObjects1
                    .stream()
                    .filter(originObject ->
                            originObject.getWater_point_condition()
                                    .equalsIgnoreCase("broken"))
                    .collect(Collectors.toList());
            int numberWaterPoints = originObjects1.size();
            double ranking = brokenWaterPointOfCurrentCommunity.size() / (double) numberWaterPoints * 100;
            value.put("number_water_point", numberWaterPoints + "");
            value.put("ranking_of_broken_water_point", String.format("%.2f", ranking).concat("%"));
            data.put(s, value);
        });
        return data;
    }

    private List<OriginObject> filterByFunctioningWater() {
        return originObjects.stream().filter(originObject ->
                originObject.getWater_functioning()
                        .equalsIgnoreCase("yes"))
                .collect(Collectors.toList());
    }

    public void saveToJson(String locationPath) {
        try {
            FileWriter fileWriter = new FileWriter(locationPath);
            fileWriter.write(calculatedToJson());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

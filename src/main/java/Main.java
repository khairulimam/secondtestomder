import com.google.gson.GsonBuilder;
import magic.WaterDataOperations;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Created by khairulimam on 23/04/17.
 */
public class Main {
    public static void main(String[] args) throws MalformedURLException {
        URL onlineURL = new URL("https://raw.githubusercontent.com/onaio/ona-tech/master/data/water_points.json");
        URL localURL = Main.class.getResource("water_points.json");
        WaterDataOperations waterDataOperations = new WaterDataOperations(onlineURL);
        System.out.println(waterDataOperations.calculatedToJson());
        waterDataOperations.saveToJson("processedData.json");
    }
}
